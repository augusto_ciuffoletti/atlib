# A wrapper for the AT interface of the ESP8266 #

This library provides an Arduino/C++ API for ESP8266 based boards. It allows the connection between an Arduino board and an ESP8266 board, using the former for data acquisition/processing/actuation, and the latter as WiFi interface. Splitting the two activities is convenient since communication has a heavy footprint and requires sophisticated algorithms that are offloaded to the specialized ESP device, that is shipped with an open source implementation of the TCP/IP stack, that is accessible through the serial interface.

An (extensible) set of commands allows to control the network interface with operations like pinging, registering with an access point, TCP communication etc. The command language reminds the Hayes command set since all commands start with an "AT+" prefix. Like the Hayes command set, the protocol for each command is that of a request/reply. Both the request and the reply go across a serial link between the two devices. The documentation about the ESP8266 AT Instruction Set is available at this [link](https://espressif.com/en/content/esp8266-instruction-set), and for this project I used Version 1.3.0.2. 

The advantage of this library with respect to WiFiESP[WiFiESP](https://github.com/bportaluri/WiFiEsp) is its memory footprint: in a typical application, with various HTTP interactions with a ThingSpeak server, processing the HTTP response body, program and data memory are around 40%, which leaves a lot of space fo sensors. For instance, the example `l55` takes respectively 39% and 33%.
 
A drawback of this design is in the limited performance of the emulated serial device, but the focus of the whole project is on low throughput communication. 

### Low power and low cost with Arduino Mini-Pro

For my project I used an Arduino Mini-Pro and an ESP-01 board. The two devices together cost less 10$. Power supply is simplified if the Mini-pro runs at 3.3V/8MHz. If you envision a breadboard prototype, keep in mind that the ESP-01 requires a breakout board. To use a 5V supply a [statement](https://www.facebook.com/groups/1499045113679103/permalink/1731855033731442/?hc_location=ufi)  of EspressIf CEO, says that ESP8266 I/O is TTL tolerant, but you need to provide the ESP01 with 3.3V power supply.

The Mini-Pro is equipped with a single UART that controls the RX/TX pins. This serial interface is also used for flashing the code, and for debugging. Using this same interface for communication with the ESP8266 board exhibits several drawbacks: during the flash operation the wiring needs to be modified, and it is more difficult to monitor the operation of the Arduino. Since I'm mostly interested in development, I opt for a software emulation provided by the SoftwareSerial library.

Using a Mini-Pro, and taking advantage of the power-saving options of the Arduino, and of the ESP01, it is possible to obtain a power consumption of a few 'mW', if communication occurs every 15' or such,

### Instant prototyping with a protoshield

The library has been tested also on Arduino Uno and the Elegoo clone.

Using a protoshield wired as in the [hackaday project](https://hackaday.io/project/107721-a-protoshield-as-a-wifi-shield) it is possible to have a running prototype in minutes.

# API reference
The wrapper is implemented as a class named *ESP*. An instance in that class is typically instantiated as a global object with:

     ESP(int rx, int tx, int baudrate, int chpd);

that takes as arguments 

* the numbers of the pins used on the Arduino for the RX and TX of the SoftwareSerial
* the baudrate of the SoftwareSerial 
* the pin on Arduino that is connected with the CHPD pin of the ESP-01, useful for power-saving

The user must define the buffers used for communication. At the moment there is **no check** on buffer overflow.

You can use the following methods in you sketch: many of them have a `timeout` parameter, in seconds, that is used to terminate the command when when it takes more than that. 

* `boolean atcmd (char cmd[], int timeout)` --- executes the AT command stored in the `cmd` and returns the response in the same buffer. The user is responsible to allocate enough space in the buffer for the command (there is **no check** on buffer overflow). The command terminates either for a timeout, or after reading a line terminating with `OK`. The return value is `true`in the first case, `false` in the other.
 * `boolean parsehttp (char buf[], int timeout)`--- manages an HTTP response. The command parses on the flight an HTTP response, discarding the header, except the *reason-phrase* that is matched with `OK`, and recording the body in the `buf` buffer (there is **no check** on buffer overflow).  The command terminates either for a timeout, or when all characters in the response are read. The return value is `true` if the *reason-phrase* is *OK*, `false` otherwise.
* `boolean ESP::readline (char patt[], int timeout)` --- reads and discards characters from the serial line, returning at the end of the line or when the timeout occurs. The return value is `true` if the pattern `patt` is found, otherwise `false`.
* `boolean ESP::sendline (char line[], int timeout)` --- sends a single `line` using the `AT+SENDEX` command. The method is a wrapper of the `atcmd` method;
* `unsigned long int ESP::daytime (const char *server, int timeout)` --- returns the time since January 1st 1970 in seconds (aka Unix time). For this it queries a server providing the TIME service on port 37. Use accurate [NIST servers](https://tf.nist.gov/tf-cgi/servers.cgi)  or [build your own](https://superuser.com/questions/311624/how-to-enable-daytime-service-on-ubuntu). The value can be used by the Time library.
* `boolean ESP::powerUp (int timeout, boolean (*esp_setup)())` --- uses the `CH_PD` pin to switch on the ESP-01, and waits for the `GOT IP` message until the timeout expires. Therefore, it depends on the correct configuration of WiFi credentials in the flash memory. The user-defined `esp_setup` function is executed after the "GOT IP" message is received. Returns true if the "GOT IP" message is received, false otherwise.
* `void ESP::powerDown()` --- uses the `CH_PD` pin to switch off the ESP-01

### How to install the library ###

The *atlib* library depends on the SoftwareSerial library, so you need to download and install it. I used version 1.0, notably based on the NewSoftSerial.

You can use the Arduino IDE to run the examples. Download this repository as a ZIP file, and, in the Arduino IDE, select

* * *
Sketch->#include library->Add library from .ZIP
* * *

Now navigate to the zip file you downloaded in the previous step and type the OK button in the Arduino/IDE GUI.

The library shows up in the "#include library" menu, and more important, the examples are available in the menu

* * *
File->Examples->Examples from custom libraries
* * *

You should find in the *atlib* folder the four examples that I have used to test the library. Pin configuration (RX=9,TX=8, CH_PD=10) are those found in the hardware prototype found in [hackaday project](https://hackaday.io/project/107721-a-protoshield-as-a-wifi-shield) :

* GET: periodically sends an HTTP GET request to the Thingspeak server with a feed consisting of an incremental number. If the operation is successful, the body content is printed (define your CHANNEL_KEY in the secret.h file)
* POST: same as GET, but using a POST
* minimal: sends a AT+GMR command (modifiy the code to change the command)
* setAP: flashes in the memory of the ESP the credentials to join the WiFi AP.
 
### How to run an example ###

The board is usually shipped with a baudrate of 115200, which is incompatible with the SoftwareSerial library, and in a "Station+AP" configuration. Inorder to use the atlib library these configurations need to be updated as follows:

* Connect the ESP01 to an FTDI232 adapter. You SHOULD use one that provides 3.3V, but your ESP-01 might survive if you use a 5V one only for the two commands that follow. Connect the GND and Vcc of the two boards, the RX and TX of the two devices (RX to TX and viceversa), and connect to Vcc the GPIO0 pin and the EN/CHPH of the ESP-01 (this you can wirewrap).
* Configure your ESP-01 as a WiFi *Station*, with the command:

		AT+CWMODE=1

* Send the ESP-01 the AT command to set the baudrate to one that is compatible with the Arduino SoftwareSerial. In order to run the examples, set it to 19200:

		AT+CIOBAUD=19200
* Detach the FTDI232

Now your ESP01 is ready to be used with the atlib library, but you need to connect it to the Arduino. For this, you can use the project in [hackaday project](https://hackaday.io/project/107721-a-protoshield-as-a-wifi-shield), and proceed as usual:

* Connect a USB of the PC using the the USB plug on the Arduino
* Go to the Arduino/IDE and select the appropriate serial port
* Install the library as explained above
* Find an example, and complete the configuration of the `secret.h` file

To connect the device with your AP, it is convenient to write the credentials in the flash memory of the ESP and configure an automatic connection. The sketch SetAP that you find among the examples helps you: configure your ESSID and PSK in the `secret.h` file, and flash the program on the Arduino. The SetAP sketch flashes your credentials in the ESP. Next it attempts to join the AP, and starts pinging google. At this point you can avoid the join operation in your sketches, since the ESP automatically joins the AP when you call the powerUp method.

In the case of *Thingspeak* examples you need to create your account and create a channel. Next, copy the *Write API Key* in the `secret.h` file, open the serial monitor and flash the example.

If you want to inspect the communication between the Arduino and the ESP, uncomment the `#define DEBUG` instruction in the *atlib.cpp* library file.

### Change history ###

## From 1.0 to 2.0
The library has been re-engineered, although the basic idea is the same: to give an application interface that makes the AT+ commands easy to use, in a more or less transparent way.
The new library introduce a different interface, so there is no backward compatibility, but I guess this is not a problem in this case. Instead, the new library solves some relevant issues of the previous version, that are also discussed in the published paper at WiCom 2017, in Rome:

* Instability: version 1.0 incurs in spontaneous resets every now and then, impredicatable. The re-boot is safe, but inacceptable for some applications. Instead, the current version runs for days. The setup is the same (same AP, same IoT server), but reboots do not occur anymore. The problem was probably in the management of the HTTP response, whose parsing was incomplete in some cases;

* Memory footprint: the new version does not need space to record the HTTP header in the response, which is parsed on the fly. The data contained in the header seems hardly of interest, and we limit to the detection of the presence of the OK in the *startline*.

* Power consumption: the previous library did not give tools to switch to sleep mode the ESP-01, while the new version takes into account various aspects related to this feature. The power consumption improves dramatically, from a few hours to days. with a pack of rechargeable AAA batteries. The switch from The Nano to the Mini-Pro also helps, making the design more compact.

* Time reference: the previous library gave no tools to lock the operation to time of the day, which is needed in many applications. So I introduced a call that returns the Unix time, using the TIME protocol (port 37). It's not as accurate and smart as NTP, but it's response is very fast to manage. The result can be managed by the usual Time.h library.

## From 2.0 to 3.0

* Removal of the `echo` parameter in favor of a #define DEBUG in the library
* Examples:
	* previous examples improved, especially `minimal` and `SetAP`
	* examples compatible with a prototype easy to assemble, good for teaching
	* more complex example from a research paper (submitted) 

## Known issues (not bugs)

* there is no buffer overflow control, that should be added to two methods

* wrapper methods should use a default timeout (reduces parameters to functions)

* use available() instead of read() in reading loops, just for style
 
### Contacts ###

* augusto.ciuffoletti@gmail.com
* Follow me on twitter @aciuffoletti
