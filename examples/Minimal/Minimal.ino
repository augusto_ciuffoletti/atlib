#include <atlib.h>
ESP esp1(9,8,19200,10); // Creazione del esp1: RX,TX,baudrate,CH_PD
void setup() {
  Serial.begin(57600);
}
void loop() {
  delay(2000);
  if ( esp1.atcmd("AT+GMR",10) ) { // Send command and print response
    Serial.println("OK");
  } else {
    Serial.println("Fail");
  }
}
