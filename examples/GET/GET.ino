#include "secret.h"
#include <atlib.h>
ESP esp1(9,8,19200,10); // RX,TX,baudrate,CH_PD
boolean pup() { true;}
void setup() {
  esp1.powerUp(30,pup);
  Serial.begin(57600);    // Arduino UART baudrate
}

unsigned long n=0;
void loop() {
  char header[100]; // Buffer to store AT command
  esp1.atcmd("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",80",5);
  sprintf(header, "POST /update?api_key=%s&field1=%d HTTP/1.1\r\n"
                  "Host: api.thingspeak.com\r\n\r\n",
                  CHANNEL_KEY,n++);
  esp1.sendline(header,5);
  if ( esp1.parsehttp(header,5) ) { 
    Serial.print("GET successful!\nBody content is: "); 
    Serial.println(header);
  } else {
    Serial.println("\nPOST failed");
  }
  esp1.readline("CLOSED",5);
  delay(60000); //un dato al minuto
}
