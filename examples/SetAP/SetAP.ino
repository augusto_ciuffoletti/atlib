#include "secret.h"
#define DEBUG
#include <atlib.h>
boolean pup() { true;}
ESP esp1(9,8,19200,10);  // RX,TX,CHPD

void setup() {
  char cmd[500];
  boolean resp;
  Serial.begin(57600);      // UART baudrate 57600
  Serial.println("Configuring ESP01 WiFi credentials");
  sprintf(cmd,"AT+CWJAP_DEF=\"%s\",\"%s\"",ESSID,PSK);
  Serial.println(cmd);
  delay(500);
  if ( ! esp1.atcmd(cmd,30) ) {
    Serial.println("Configuration failed");
    while (true) {}
  }
  esp1.powerDown();
}


void loop() {
  delay(10000);
  esp1.powerUp(30,pup);
  Serial.print("Pinging Google...");
  if ( esp1.atcmd("AT+PING=\"www.google.com\"",2) ) {
    Serial.println("Ok");
  } else {
    Serial.println("Fail");
  }
  esp1.powerDown();
}
