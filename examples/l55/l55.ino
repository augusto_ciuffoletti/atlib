/*
 * Questo programma è destinato a verificare il prototipo
 * per l'articolo su rivista del LAVORO55. 
 * Alimentazione con tre pile AAA 800mAh (valutare stepdown)
 * Dovrebbe pubblicare:
 * *) indice crescente
 * *) offset del clock locale
 * *) qualche misura
 * Dovrebbe scaricare un comando di attuazione
 */
#define LOWPOWER_SBC
//#define DEBUG
#ifdef LOWPOWER_SBC
#include <LowPower.h>
#endif
#include "secret.h"
#define FR A0
#define NTC A1
#define LED 13
#define D_SAMPLE 2L     // schedule period (secs)
#define D_COMM 900L      // communication period (secs)
#include <atlib.h>


char timestamp[30];      // Buffer for the timestamp
// boolean sync=false;
// timing variables
unsigned long syncdata0[3]; // 0: clock, 1:utc
unsigned long syncdata[3];  // 0: clock, 1:utc
float drift=1.0;
boolean sync=false;
unsigned long t_sample, t_comm;
unsigned long uptime_ESP=0;
unsigned long uptime_ARD=0;
// Actuator variables
unsigned int t_led, d_led;
// Sensor variables
float light;
int n_light;
float temperature;
int n_temperature;

unsigned long n=0;

ESP esp1(10,9,19200,11); // RX,TX,baudrate,CH_PD
#define NOW esp1.utc(syncdata0,drift)

boolean pup() { true;}

void setup() {
#ifdef DEBUG
  Serial.begin(57600);       // Arduino UART baudrate
#endif
  pinMode(LED,OUTPUT);
  digitalWrite(LED,0);
  esp1.powerUp(30,pup); // Synchronize clock
  while ( ! esp1.getsync("time.nist.gov",20,syncdata0) ) {};
  esp1.readline("CLOSED",5);
  esp1.powerDown();
  t_comm=NOW;
  t_sample=NOW;
}

long readVcc() {
  // https://provideyourown.com/2012/secret-arduino-voltmeter-measure-battery-voltage/
  // Read 1.1V reference against AVcc
  // set the reference to Vcc and the measurement to the internal 1.1V reference
  #if defined(__AVR_ATmega32U4__) || defined(__AVR_ATmega1280__) || defined(__AVR_ATmega2560__)
    ADMUX = _BV(REFS0) | _BV(MUX4) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #elif defined (__AVR_ATtiny24__) || defined(__AVR_ATtiny44__) || defined(__AVR_ATtiny84__)
    ADMUX = _BV(MUX5) | _BV(MUX0);
  #elif defined (__AVR_ATtiny25__) || defined(__AVR_ATtiny45__) || defined(__AVR_ATtiny85__)
    ADMUX = _BV(MUX3) | _BV(MUX2);
  #else
    ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  #endif  

  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Start conversion
  while (bit_is_set(ADCSRA,ADSC)); // measuring

  uint8_t low  = ADCL; // must read ADCL first - it then locks ADCH  
  uint8_t high = ADCH; // unlocks both

  long result = (high<<8) | low;

  result = 1125300L / result; // Calculate Vcc (in mV); 1125300 = 1.1*1023*1000
  return result; // Vcc in millivolts
}

int comm() {
  char body[100]=""; // Buffer to record http body
  char header[150]; // Buffer to store AT command
  char temperature_str[10];
  int result=0;
  unsigned long error=0;
  unsigned long t_up=millis();
  int d_led;
  esp1.powerUp(30,pup);
  // One shot: if fail syncdata[2] = timeout
  esp1.getsync("time.nist.gov",20,syncdata);
  error=syncdata[1]-NOW;
//  float drift0=(syncdata[1]-syncdata0[1])/((float)(syncdata[0]-syncdata0[0]));
//  drift=(sync)?(drift0+(3*drift))/4:drift0;
  drift=(syncdata[1]-syncdata0[1])/((float)(syncdata[0]-syncdata0[0]));
  sync=true;
  esp1.readline("CLOSED",5);
// get command
  esp1.atcmd("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",80",5);
  sprintf(header, "GET /talkbacks/%s/commands/execute?api_key=%s HTTP/1.1\r\n"
                  "Host: api.thingspeak.com\r\n\r\n",TALKBACK_ID,TALKBACK_KEY);
  esp1.sendline(header,5);
  if ( esp1.parsehttp(header,5) ) {
    if ( strlen(header) > 0 ) {
#ifdef DEBUG
      Serial.print("led on time received: ");
      Serial.println(header);
#endif
      d_led=atoi(strtok(header,","));
      result=d_led;
    }
  } else {
#ifdef DEBUG
    Serial.println("\nGET failed");
#endif
  }
  esp1.readline("CLOSED",5);
// feed channel
  dtostrf(temperature/n_temperature,4,2,temperature_str);
  long printDrift=(int)((drift-1)*1000000);
#ifdef DEBUG
  Serial.println(printDrift);
#endif
  esp1.atcmd("AT+CIPSTART=\"TCP\",\"api.thingspeak.com\",80",5);
  sprintf(body,"api_key=%s&field1=%ld&field2=%ld&field3=%ld&field4=%d&field5=%d&field6=%ld&field7=%ld&field8=%ld\r\n",
                CHANNEL_KEY,
                n++,                          // 1 n
                printDrift,                   // 2 drift
                (unsigned long)syncdata[2],   // 3 roundtrip
                (int)error,                   // 4 clock error
                (int)((light/n_light)*1000),  // 5 light
                uptime_ARD,                     // 6 uptime (Arduino)
                uptime_ESP,                       // 7 uptime (ESP)
                readVcc());                   // 8 Vcc
// initialize accumulators
  light=0; n_light=0;
  temperature=0; n_temperature=0;
  sprintf(header, "POST /update HTTP/1.1\r\n"
                  "Host: api.thingspeak.com\r\n"
                  "Connection: close\r\n"
                  "Content-Type: application/x-www-form-urlencoded\r\n"
                  "Content-Length: %d\r\n\r\n",strlen(body));
  esp1.sendline(header,5);
  esp1.sendline(body,5);
#ifdef DEBUG
  if ( esp1.parsehttp(body,5) ) { 
    Serial.print("POST successful!\nBody content is: "); 
    Serial.println(body);
  } else {
    Serial.println("\nPOST failed");
  }
#endif
  esp1.readline("CLOSED",5);
  esp1.powerDown();
  uptime_ESP+=millis()-t_up;
  uptime_ARD=millis();
  return result;
}

float read_temp() {
        float r = 12000;
        float a = -19.39;
        float b = 201.38+3.4;
        return a*log(r/((1024.0/analogRead(NTC))-1))+b;
}

/* 
 *  Sends a POST with an incremental value to a ThingSpeak server
 *  Each round the the ESP 01 and the Mini are switched on and off. 
 *  The response is parsed to extract http exit code and body
 *  Long run test ...
 */
void loop() {
#ifdef LOWPOWER_SBC
  for ( int i=0; i<2; i++ ) {
    LowPower.powerDown(SLEEP_1S, ADC_OFF, BOD_OFF);
  }
  syncdata0[1]+=D_SAMPLE;
#else /* not LOWPOWER_SBC */
  while ( t_sample+D_SAMPLE >= NOW ) {
  }
#endif /* not LOWPOWER_SBC */
//  Serial.println(NOW);
//  Serial.println(n);
  digitalWrite(LED,! digitalRead(LED));
  t_sample+=D_SAMPLE;
//  Serial.println(t_comm+D_COMM);
// Sampling
  light=light +(analogRead(FR)/1023.0); n_light++;
  temperature=temperature+read_temp(); n_temperature++;
// Communication
  if ( t_comm + D_COMM <= NOW ) {
    t_comm+=D_COMM;
    d_led=comm();
// Actuator
    if ( d_led > 0 ) { 
      digitalWrite(13,true);
      t_led=NOW;
    }
  }
  if ( digitalRead(13) ) {
    if ( ( t_led + d_led ) < NOW ) {
      digitalWrite(13,false); 
    }
  }
}
